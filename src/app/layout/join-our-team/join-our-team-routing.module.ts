import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { JoinOurTeamComponent } from './join-our-team.component';


const routes: Routes = [{
  path: '',
  component: JoinOurTeamComponent
}];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class JoinOurTeamRoutingModule { }
