import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { JoinOurTeamRoutingModule } from './join-our-team-routing.module';


@NgModule({
  declarations: [],
  imports: [
    CommonModule,
    JoinOurTeamRoutingModule
  ]
})
export class JoinOurTeamModule { }
