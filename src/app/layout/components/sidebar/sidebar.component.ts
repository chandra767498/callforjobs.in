import { Component, OnInit } from '@angular/core';
import { CookieService } from 'ngx-cookie-service';
import { Observable, interval, Subscription } from 'rxjs';
import { AppService } from 'src/app/app.service';
import { Router, NavigationEnd } from '@angular/router';

@Component({
    selector: 'app-sidebar',
    templateUrl: './sidebar.component.html',
    styleUrls: ['./sidebar.component.scss'],
})
export class SidebarComponent implements OnInit {
    isActive = false;
    collapsed = false;
    public showMenu: string;
    public activeEle: string;
    currentRoute: String;
    user_type: string;
    pushRightClass = 'push-right';
    newColor = false;
    adminData: any = {};
    permissions: any = {};
    is_admin: Boolean;
    count: any = {};
    showVar = true;
    private updateSubscription: Subscription;
    constructor(
        private cookieService: CookieService,
        private appService: AppService,
        public router: Router,

    ) {
        if (this.cookieService.get('adminData')) {
            this.adminData = JSON.parse(this.cookieService.get('adminData'));
        }
        // else{
        //     this.adminData = {};
        //     this.router.navigate(["/login"]);
        // }
        this.router.events.subscribe(val => {
          this.currentRoute = this.router.url;
          this.showMenu = '';
        //   console.log('currentRoute', this.currentRoute);
        //   console.log('showMenu', this.showMenu);
          if (
            val instanceof NavigationEnd &&
            window.innerWidth <= 992 &&
            this.isToggled()
          ) {
            this.toggleSidebar();
          }
        });
      }

    ngOnInit() {
        this.showMenu = '';
        if (this.adminData.permissions) {
            this.permissions = this.adminData.permissions;
        } else {
            this.permissions = {};
        }
    }
  
    addExpandClass(element: any) {
        if (element === this.showMenu) {
            this.showMenu = '0';
        } else {
            this.showMenu = element;
        }
    }
    toggleColor() {
        this.newColor = !this.newColor;
    }
    image() {
        console.log('Hllo');
        this.showVar = !this.showVar;
    }
    isToggled(): boolean {
        const dom: Element = document.querySelector('body');
        return dom.classList.contains(this.pushRightClass);
      }

      toggleSidebar() {
        const dom: any = document.querySelector('body');
        dom.classList.toggle(this.pushRightClass);
      }
}
