import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { TopnavRoutingModule } from './topnav-routing.module';
// import { ProfileComponent } from './profile/profile.component';


@NgModule({
  declarations: [],
  imports: [
    CommonModule,
    TopnavRoutingModule,
    // ProfileComponent,
  ]
})
export class TopnavModule { }
