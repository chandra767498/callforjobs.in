import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { JobApplyComponent } from './job-apply.component';


const routes: Routes = [{
  path: '',
  component : JobApplyComponent
}];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class JobApplyRoutingModule { }
