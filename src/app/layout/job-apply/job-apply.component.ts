import { Component, OnInit } from '@angular/core';
import { FormsModule, FormControl } from '@angular/forms';
import { AppService } from 'src/app/app.service';
import { CookieService } from 'ngx-cookie-service';
import { Router } from '@angular/router';
import swal from 'sweetalert2';
import {
    NgbModal,
    NgbModalConfig,
    ModalDismissReasons,
} from '@ng-bootstrap/ng-bootstrap';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';

@Component({
    selector: 'app-job-apply',
    templateUrl: './job-apply.component.html',
    styleUrls: ['./job-apply.component.scss'],
    providers: [NgbModalConfig, NgbModal],
})
export class JobApplyComponent implements OnInit {
    userData: any = {};
    RegistrationForm: FormGroup;
    education = '';
    form: any = {};
    adminData: any;
    experience: any;
    dataShow: boolean;

    constructor(
        private appService: AppService,
        private cookieService: CookieService,
        private modalService: NgbModal
    ) {
    }

    ngOnInit() {
        this.userData.education = 'belowssc';
        this.userData.experience = '0';
        this.dataShow = false;
    }
    applyJob() {
        const data = {
            name: this.userData.name,
            email_id: this.userData.email_id,
            mobile_number: this.userData.mobile,
            education: this.userData.education,
            experience: this.userData.experience,
            previous_company: this.userData.previous_company,
            previous_role: this.userData.previous_role,
        };

        console.log(data);
        try {
          this.appService.postMethod('users', data)
            .subscribe((resp: any) => {
              if (resp.success) {
                this.userData = {};
                swal.fire(' Created Succesfully');

              } else {
                swal.fire('Enter All Tags', 'Something went wrong!', 'error');
              }
            },
              error => {
              });
        } catch (e) { }
    }

    statusChnage() {
        console.log('changed', this.userData.experience);
        if (this.userData.experience > 0) {
            this.dataShow = true;
        } else {
            this.dataShow = false;
        }
    }

}
