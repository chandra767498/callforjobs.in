import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { FormsModule } from '@angular/forms';
import { MatInputModule } from '@angular/material/input';
import {MatTabsModule} from '@angular/material/tabs';
import {MatCardModule} from '@angular/material/card';
import {MatSelectModule} from '@angular/material/select';
import {MatIconModule} from '@angular/material/icon';
import { ReactiveFormsModule } from '@angular/forms';
import { JobApplyRoutingModule } from './job-apply-routing.module';
import { JobApplyComponent } from './job-apply.component';


@NgModule({
  declarations: [JobApplyComponent],
  imports: [
    CommonModule,
    JobApplyRoutingModule,
    FormsModule,
    ReactiveFormsModule,
    MatIconModule,
    NgbModule,
    MatSelectModule,
    MatCardModule,
    MatTabsModule,
    MatInputModule,
  ]
})
export class JobApplyModule { }
