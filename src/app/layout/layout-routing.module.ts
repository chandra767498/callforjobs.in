import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { LayoutComponent } from './layout.component';
const routes: Routes = [
    {
        path: '',
        component: LayoutComponent,
        children: [
            {
                path: '',
                redirectTo: 'home'
            },
            {
                path: 'home',
                loadChildren: () => import('./dashboard/dashboard.module').then(m => m.DashboardModule)
            },
            {
                path: 'job-apply',
                loadChildren: () => import('./job-apply/job-apply.module').then(m => m.JobApplyModule)
            },
            {
                path: 'about-us',
                loadChildren: () => import('./about-us/about-us.module').then(m => m.AboutUsModule)
            },
            {
                path: 'services',
                loadChildren: () => import('./services/services.module').then(m => m.ServicesModule)
            },
            {
                path: 'join-our-team',
                loadChildren: () => import('./join-our-team/join-our-team.module').then(m => m.JoinOurTeamModule)
            },
            {
                path: 'media',
                loadChildren: () => import('./media/media.module').then(m => m.MediaModule)
            },
            {
                path: 'contact-us',
                loadChildren: () => import('./contact-us/contact-us.module').then(m => m.ContactUsModule)
            },
        ],
    }
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule],
})
export class LayoutRoutingModule {}
