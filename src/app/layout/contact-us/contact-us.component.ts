import { Component, OnInit } from '@angular/core';
import {  FormsModule } from '@angular/forms';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { AppService } from 'src/app/app.service';
import { CookieService } from 'ngx-cookie-service';
import { Router } from '@angular/router';
import swal from 'sweetalert2';
import {
    NgbModal,
    NgbModalConfig,
    ModalDismissReasons,
} from '@ng-bootstrap/ng-bootstrap';
@Component({
  selector: 'app-contact-us',
  templateUrl: './contact-us.component.html',
  styleUrls: ['./contact-us.component.scss']
})
export class ContactUsComponent implements OnInit {
  ContactForm: FormGroup;
  userData: any;
  contactData: any = {};
  constructor(
    private appService: AppService,
    private cookieService: CookieService,
    private modalService: NgbModal
  ) { }

  ngOnInit(): void {
  }
  contact() {
    const data = {
      name: this.contactData.name,
      email_id: this.contactData.email,
      mobile_number: this.contactData.mobilenumber,
      address: this.contactData.address,
      Message: this.contactData.Message,
  };
  console.log(data);
   try {
          this.appService.postMethod('queries', data)
            .subscribe((resp: any) => {
              if (resp.success) {
                // this.userData = {};
                swal.fire('Message sent');

              } else {
                // swal.fire('Enter All Tags', 'Something went wrong!', 'error');
              }
            },
              error => {
              });
        } catch (e) { }
  }
}
