import { isPlatformBrowser } from '@angular/common';
import { HttpClient, HttpHeaders, HttpParams, HttpRequest } from '@angular/common/http';
import { Inject, Injectable, PLATFORM_ID } from '@angular/core';
import { Headers, Http, RequestOptions } from '@angular/http';
import { Router } from '@angular/router';
import { CookieService } from 'ngx-cookie-service';
import { map } from 'rxjs/operators';
import { environment } from '../../../../environments/environment';





@Injectable({
  providedIn: 'root'
})
export class DataService {
  apiurl: string = environment.apiurl;
  // userTags: any;

  constructor(private http: Http, private router: Router, private httpclient: HttpClient,
    @Inject(PLATFORM_ID) public platformId: object, 
    private cookieService: CookieService,
  ) {
    if (isPlatformBrowser(this.platformId)) {
      // this.userTags = new TruePushUserTags('5bbf4602a24730578b7e6ee9');
    }
  }

  post(url, body) {
    return this.http.post(this.apiurl + url, body)
      .pipe(map((res) => res.json()));
  }

  get(url) {
    return this.http.get(this.apiurl + url)
      .pipe(map((res) => res.json()));
  }

  getAuth(url) {
    const headers = new Headers();
    const token = this.cookieService.get('token');
    headers.append('x-access-token', token);
    const opts = new RequestOptions();
    opts.headers = headers;
    return this.http.get(this.apiurl + url, opts)
      .pipe(map((res) => {
        return res.json();
      }));
  }

  getAuthParams(url, params) {
    const headers = new Headers();
    const token = this.cookieService.get('token');

    headers.append('x-access-token', token);
    const opts = new RequestOptions();
    opts.headers = headers;
    opts.params = params;

    return this.http.get(this.apiurl + url, opts)
      .pipe(map((res) => {
        return res.json();
      }));
  }
  getAuthParamsNew(url, params) {
    const headers = new Headers();
    const token = this.cookieService.get('token');

    headers.append('x-access-token', token);
    const opts = new RequestOptions();
    opts.headers = headers;
    opts.params = params;

    return this.http.get(this.apiurl + url, opts)
      .pipe(map((res) => {
        return res.json();
      }));
  }

  getParams(url, params) {
    const opts = new RequestOptions();
    opts.params = params;
    return this.http.get(this.apiurl + url, opts)
      .pipe(map((res) => res.json()));
  }

  postAuth(url, body) {
    const headers = new Headers();
    const token = this.cookieService.get('token');

    headers.append('x-access-token', token);
    const opts = new RequestOptions();
    opts.headers = headers;

    return this.http.post(this.apiurl + url, body, opts)
      .pipe(map((res) => {
        return res.json();
      }));
  }

  // expireToken(res, notjson) {
  //   if ('' + res.json().status === 'AUTHTOKENERR') {
  //     if (localStorage.getItem('p_name') != null) {
  //       localStorage.removeItem('p_name');
  //     }
  //     if (localStorage.getItem('p_au_token') != null) {
  //       localStorage.removeItem('p_au_token');
  //     }
  //     if (localStorage.getItem('p_profile_pic')) {
  //       localStorage.removeItem('p_profile_pic');
  //     }
  //     localStorage.removeItem('p_expiry');
  //     return this.router.navigate(['/']);
  //   } else {
  //     if (notjson) {
  //       return res;
  //     } else {
  //       return res.json();
  //     }
  //   }
  // }

  prepareParams(data) {
    let params = new HttpParams();
    const param_keys = Object.keys(data);
    // tslint:disable-next-line:prefer-for-of
    for (let index = 0; index < param_keys.length; ++index) {
      const key = param_keys[index];
      const value = data[key];
      params = params.append(key, value);
    }
    return params;
  }

  upload(url, file, file_key, data, progress) {
    const token = localStorage.getItem('_wf_token');

    const form_data = new FormData();
    form_data.append(file_key, file);

    const params = this.prepareParams(data);

    const request_options: any = {};
    progress ? request_options.reportProgress = true : 'no progress';
    request_options.params = params;
    const request = new HttpRequest('POST', this.apiurl + url, form_data, request_options);

    return this.httpclient.request(request);
  }

  uploadAuth(url, file, file_key, data, progress) {
    const token = localStorage.getItem('_wf_token');
    const headers = new HttpHeaders().set('x-access-token', token);

    const form_data = new FormData();
    form_data.append(file_key, file);

    const params = data ? this.prepareParams(data) : {};

    const request_options: any = {};
    // tslint:disable-next-line:no-unused-expression
    progress ? request_options.reportProgress = true : 'no progress';
    request_options.params = params;
    request_options.headers = headers;

    const request = new HttpRequest('POST', this.apiurl + url, form_data, request_options);

    return this.httpclient.request(request);
  }

  //   pixelTrack(lead_id) {
  //     const url = 'https://www.tpclick.com/rd/jpx.php?sid=2584&transid=' + lead_id;
  //     console.log('Pixel track request', url);

  //     const headers = new Headers();
  //     headers.append('Access-Control-Allow-Origin', '*');
  //     const options = new RequestOptions({
  //       headers: headers
  //     });

  //     this.http.get(url, options)
  //       .subscribe(res => {
  //         console.log('pixel response ', res);
  //       });
  //   }


}
